PrimeFinder is a simple application running on Spring shell.

It provides tools to find primes.

Currently available commands:  
- search-excel - takes a path to an excel file as and argument and prints all primes located in its second column.
