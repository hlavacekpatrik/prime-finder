package com.hlavacek.PrimeFinder.command;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.shell.Shell;
import org.springframework.shell.jline.InteractiveShellApplicationRunner;
import org.springframework.shell.jline.ScriptShellApplicationRunner;
import org.springframework.shell.result.DefaultResultHandler;

import java.io.*;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(properties = {
        InteractiveShellApplicationRunner.SPRING_SHELL_INTERACTIVE_ENABLED + "=false",
        ScriptShellApplicationRunner.SPRING_SHELL_SCRIPT + ".enabled=false"
})
class CommandsIntegrationTest {

    @Autowired
    private Shell shell;

    @Autowired
    private DefaultResultHandler resultHandler;

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();

    private final List<String> testedNumbers = List.of("7", "24", "157", "5667");

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @Test
    void searchExcel_ok() throws IOException {
        File file = mockWorkbookFile();
        Object result = shell.evaluate(() -> "search-excel " + file.getAbsolutePath());

        assertEquals("7\r\n" +
                "157\r\n", outContent.toString());
    }

    @Test
    void searchExcel_invalidFormat_printedError() throws IOException {
        File file = File.createTempFile("test", ".png");
        Object result = shell.evaluate(() -> "search-excel " + file.getAbsolutePath());

        assertEquals(Commands.INVALID_FILE_ERROR + "\r\n", errContent.toString());
    }

    private File mockWorkbookFile() throws IOException {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        for (int i = 0; i < testedNumbers.size(); i++) {
            sheet.createRow(i).createCell(1).setCellValue(testedNumbers.get(i));
        }

        File file = File.createTempFile("test", ".xslx");
        FileOutputStream outputStream = new FileOutputStream(file);
        workbook.write(outputStream);

        outputStream.close();
        return file;
    }
}