package com.hlavacek.PrimeFinder.service;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PrimeServiceImplTest {

    PrimeService primeService = new PrimeServiceImpl();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    private final List<String> testedNumbers = List.of("7", "24", "157", "5667");

    @BeforeEach
    public void setUpStream() {
        System.setOut(new PrintStream(outContent));
    }

    @Test
    void printPrimesInWorkbook_ok() {
        Workbook workbook = mockWorkbook();

        primeService.printPrimesInWorkbook(workbook);

        assertEquals("7\r\n" +
                "157\r\n", outContent.toString());
    }

    @Test
    void printPrimesInWorkbook_invalidCharacters_charactersIgnored() {
        Workbook workbook = mockWorkbook();

        workbook.getSheetAt(0).createRow(testedNumbers.size()).createCell(1).setCellValue("ABCD");
        workbook.getSheetAt(0).createRow(testedNumbers.size() + 1).createCell(1).setCellValue("17.5");
        workbook.getSheetAt(0).createRow(testedNumbers.size() + 2).createCell(1).setCellValue("-29");
        primeService.printPrimesInWorkbook(workbook);

        assertEquals("7\r\n" +
                "157\r\n", outContent.toString());
    }

    private Workbook mockWorkbook() {
        Workbook workbook = new XSSFWorkbook();
        Sheet sheet = workbook.createSheet();

        for (int i = 0; i < testedNumbers.size(); i++) {
            sheet.createRow(i).createCell(1).setCellValue(testedNumbers.get(i));
        }

        return workbook;
    }
}