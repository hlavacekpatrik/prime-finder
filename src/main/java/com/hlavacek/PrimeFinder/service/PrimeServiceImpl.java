package com.hlavacek.PrimeFinder.service;

import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

@Component
@Validated
public class PrimeServiceImpl implements PrimeService {

    @Override
    public void printPrimesInWorkbook(@NonNull Workbook workbook) {

        Sheet sheet = workbook.getSheetAt(0);

        for (Row row : sheet) {

            Cell cell = row.getCell(1);

            if (cell != null) {
                String cellValue = cell.getStringCellValue();

                if (StringUtils.isNumeric(cellValue) && isPrime(Long.parseLong(cellValue))) {
                    System.out.println(cellValue);
                }
            }
        }
    }

    private static boolean isPrime(final long n) {
        // Corner cases
        if (n <= 1) {
            return false;
        } else if (n <= 3) {
            return true;
        }

        // This is checked so that we can skip
        // middle five numbers in below loop
        if (n % 2 == 0 || n % 3 == 0) {
            return false;
        }

        for (int i = 5; i * i <= n; i = i + 6) {

            if (n % i == 0 || n % (i + 2) == 0) {
                return false;
            }
        }

        return true;
    }
}
