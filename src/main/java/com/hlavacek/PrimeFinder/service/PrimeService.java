package com.hlavacek.PrimeFinder.service;

import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.lang.NonNull;

public interface PrimeService {

    /**
     * Looks thought all rows in second column of a Excel workbook and prints any prime numbers found. <br>
     * Ignores any cells that are not positive whole numbers.
     */
    void printPrimesInWorkbook(@NonNull Workbook workbook);
}
