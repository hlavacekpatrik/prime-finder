package com.hlavacek.PrimeFinder.command;

import com.hlavacek.PrimeFinder.service.PrimeService;
import org.apache.poi.EmptyFileException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.exceptions.NotOfficeXmlFileException;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.lang.NonNull;
import org.springframework.shell.standard.ShellComponent;
import org.springframework.shell.standard.ShellMethod;

import java.io.File;
import java.io.IOException;

@ShellComponent
public class Commands {

    public static final String INVALID_FILE_ERROR = "Provided file was invalid.";

    public static final String FILE_READ_ERROR = "Provided file has invalid format.";

    private final PrimeService primeService;

    public Commands(PrimeService primeService) {
        this.primeService = primeService;
    }

    @ShellMethod("Prints all primes in second column of an Excel file")
    public void searchExcel(@NonNull File file) {

        Workbook workbook;

        try {
            workbook = new XSSFWorkbook(file);
        } catch (InvalidFormatException |
                 IllegalArgumentException e) {
            System.err.println(INVALID_FILE_ERROR);
            return;
        } catch (IOException e) {
            System.err.println(FILE_READ_ERROR);
            return;
        }

        primeService.printPrimesInWorkbook(workbook);
    }
}
